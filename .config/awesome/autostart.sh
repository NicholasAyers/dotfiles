#!/usr/bin/env bash

function run {
    if ! pgrep $1 ;
    then
	$@&
    fi
}

function runif {
    if ! pgrep $1 ;
    then
	${@:2}
    fi
}



runif electron android-messages-desktop
run redshift-gtk -l 47:-122
