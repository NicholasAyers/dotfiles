b#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Personal Aliases
alias config='/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME' # Git repo for dotfiles
alias config-update="config add -u"

detach() {
  "$@" 1>/dev/null 2>/dev/null & disown
}


#
# # ex - archive extractor
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}


alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -h'                      # show sizes in MB
alias np='nano -w PKGBUILD'
alias more=less
alias ls='ls -h --color=auto'
alias grep='grep --color=auto'
PS1='[\u@\h \W]\$ '

export PATH=~/.local/bin:$PATH
complete -cf sudo
